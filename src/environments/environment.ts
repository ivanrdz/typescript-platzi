// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDfhq8D53GMOph6gICkDwp-1jc88TZUfOs',
    authDomain: 'typescript-platzi-23eed.firebaseapp.com',
    databaseURL: 'https://typescript-platzi-23eed.firebaseio.com',
    projectId: 'typescript-platzi-23eed',
    storageBucket: '',
    messagingSenderId: '1048621067695',
    appId: '1:1048621067695:web:3ed48acb6332e16b'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
